//=============================================================================
//
// file :        ImgGrabber.h
//
// description : Include for the ImgGrabber class.
//
// project :	ImgGrabber
//
// $Author: sergiblanch $
//
// $Revision: 1.6 $
//
// $Log: not supported by cvs2svn $
// Revision 1.5  2011/05/25 09:05:47  vince_soleil
// 19247 : Yat migration
//
// Revision 1.4  2010/09/15 16:57:03  vince_soleil
// merged from maven migration branch (last commit)
//
// Revision 1.1.2.4  2010/09/14 13:02:24  anoureddine
// Avoid a shift of 1 point during scan step by step :
// - Enhance state management (forcing state to RUNNING when a snap command is launched)
// - Add some traces
//
// Revision 1.1.2.3  2010/04/20 11:28:43  vince_soleil
// replaced by version from HEAD
//
// Revision 1.10  2010/04/07 13:25:30  ollupac
// Now the Slot gets a SharedImage
//
// This way it can decide to make a cheap duplicate and process it asynchronously.
//
// Revision 1.9  2010/04/07 13:24:21  ollupac
// Add events to ImageCounter
//
// Revision 1.8  2009/03/26 15:28:53  julien_malik
// include Ramon Sune evolutions :
// - direct memory-link with ImgBeamAnalyzer
// - PylonV2 Basler plugin
// - synchronization fixes
// - ImageCounter attribute
// - linux compilation
//
// Revision 1.7  2008/05/20 14:08:03  julien_malik
// modification of the IMAQ plugin to have 1 to 4 devices in one dserver instance, one per channel.
// big modification of the dynamic attributes management
//
// Revision 1.5  2008/03/07 11:19:31  julien_malik
// - plugins are now loaded at init and unloaded only at program exit
// - PylonInitialize & PylonTerminate are now properly called because of first item
// - GetPluginInfo implemented
//
// Revision 1.4  2008/03/05 13:31:57  julien_malik
// - add 'SaveSettings' command
// - support external trigger for Basler cameras
//
// Revision 1.3  2007/10/23 16:29:28  julien_malik
// rework the movie saving
//
// Revision 1.2  2007/10/09 14:46:42  julien_malik
// - support new plugin system
// - add AutoStart property
// - Add properties for the BaslerGrabber to save the desired init value of exposure time, frame rate, gain and black level
//
// Revision 1.1  2007/08/20 15:03:21  stephle
// initial import
//
//
// copyleft :    European Synchrotron Radiation Facility
//               BP 220, Grenoble 38043
//               FRANCE
//
//=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================
#ifndef _IMGGRABBER_H
#define _IMGGRABBER_H

#ifdef WIN32
//#pragma warning( push )
#pragma warning( disable: 4786 ) // 
#pragma warning( disable: 4267 ) // 'var' : conversion from 'size_t' to 'type', possible loss of data 
#pragma warning( disable: 4311 ) // 'variable' : pointer truncation from 'type' to 'type' 
#pragma warning( disable: 4312 ) // 'operation' : conversion from 'type1' to 'type2' of greater size 
#pragma warning( disable: 4250 ) // 'x' inherits 'y' via dominance
#endif
#include <tango.h>
#include <list>
#include <yat/utils/Signal.h>
//#include <Tools.h>
#include "IGrabber.h"

//using namespace Tango;

/**
 * @author	$Author: sergiblanch $
 * @version	$Revision: 1.6 $
 */

 //	Add your own constants definitions here.
 //-----------------------------------------------
//- FORWARD DECL  
namespace GrabAPI
{
  class IGrabber;
  class GrabberTask;
  class SharedImage;
  class MovieWriterTask;
}
namespace yat
{
  class PlugInManager;
  class Exception;
}
//namespace yat4tango
//{
//  class DynamicAttrHelper;
//  class PlugInHelper;
//}
namespace isl
{
  class Image;
}


namespace ImgGrabber_ns
{

/**
 * Class Description:
 * 
 */

/*
 *	Device States Description:
*  Tango::OPEN :
*  Tango::CLOSE :
*  Tango::RUNNING :
*  Tango::STANDBY :
*  Tango::FAULT :
*  Tango::ALARM :
 */


class ImgGrabber: public Tango::Device_3Impl
{
public :
	//	Add your own data members here
	//-----------------------------------------


	//	Here is the Start of the automatic code generation part
	//-------------------------------------------------------------	
/**
 *	@name attributes
 *	Attributs member data.
 */
//@{
		Tango::DevBoolean	*attr_CurrentlySavingMovie_read;
		Tango::DevString	*attr_MovieRemainingTime_read;
		Tango::DevUShort	*attr_Image_read;
//@}

/**
 *	@name Device properties
 *	Device properties member data.
 */
//@{
/**
 *	the full path to the ImgGrabber plugin
 */
	string	pluginLocation;
/**
 *	the path where the movie/images are recorded
 */
	string	moviePath;
/**
 *	the format of the saved files : JPG, PNG, TIF, PGM, BMP, DIB, RAS
 */
	string	movieFormat;
/**
 *	automatically start grabbing at init
 */
	Tango::DevBoolean	autoStart;
	Tango::DevBoolean   autoOpen;
//@}

/**@name Constructors
 * Miscellaneous constructors */
//@{
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device Name
 */
	ImgGrabber(Tango::DeviceClass *cl,string &s);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device Name
 */
	ImgGrabber(Tango::DeviceClass *cl,const char *s);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device name
 *	@param d	Device description.
 */
	ImgGrabber(Tango::DeviceClass *cl,const char *s,const char *d);
//@}

/**@name Destructor
 * Only one desctructor is defined for this class */
//@{
/**
 * The object desctructor.
 */	
	~ImgGrabber() {delete_device();};
/**
 *	will be called at device destruction or at init command.
 */
	void delete_device();
//@}

	
/**@name Miscellaneous methods */
//@{
/**
 *	Initialize the device
 */
	virtual void init_device();
/**
 *	Always executed method befor execution command method.
 */
	virtual void always_executed_hook();

//@}

/**
 * @name ImgGrabber methods prototypes
 */

//@{
/**
 *	Hardware acquisition for attributes.
 */
	virtual void read_attr_hardware(vector<long> &attr_list);
/**
 *	Extract real attribute values for CurrentlySavingMovie acquisition result.
 */
	virtual void read_CurrentlySavingMovie(Tango::Attribute &attr);
/**
 *	Extract real attribute values for MovieRemainingTime acquisition result.
 */
	virtual void read_MovieRemainingTime(Tango::Attribute &attr);
/**
 *	Extract real attribute values for Image acquisition result.
 */
	virtual void read_Image(Tango::Attribute &attr);
/**
 *	Read/Write allowed for CurrentlySavingMovie attribute.
 */
	virtual bool is_CurrentlySavingMovie_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for MovieRemainingTime attribute.
 */
	virtual bool is_MovieRemainingTime_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for Image attribute.
 */
	virtual bool is_Image_allowed(Tango::AttReqType type);
/**
 *	Execution allowed for Open command.
 */
	virtual bool is_Open_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for Close command.
 */
	virtual bool is_Close_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for Start command.
 */
	virtual bool is_Start_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for Stop command.
 */
	virtual bool is_Stop_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for StartSaveMovie command.
 */
	virtual bool is_StartSaveMovie_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for StopSaveMovie command.
 */
	virtual bool is_StopSaveMovie_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for Snap command.
 */
	virtual bool is_Snap_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for SetROI command.
 */
	virtual bool is_SetROI_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for GetROI command.
 */
	virtual bool is_GetROI_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for ResetROI command.
 */
	virtual bool is_ResetROI_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for SaveSettings command.
 */
	virtual bool is_SaveSettings_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for GetPluginInfo command.
 */
	virtual bool is_GetPluginInfo_allowed(const CORBA::Any &any);
/**
 *  Execution allowed for Open command.
 */
    virtual bool is_ResetCamera_allowed(const CORBA::Any &any);
/**
 * 
 *	@exception DevFailed
 */
	void	open();
/**
 * 
 *	@exception DevFailed
 */
	void	close();
/**
 * 
 *	@exception DevFailed
 */
	void	start();
/**
 * 
 *	@exception DevFailed
 */
	void	stop();
/**
 * saves a movie in the directory specified in the MoviePath property, and with the format specified as MovieFormat.
 *	@param	argin	duration of the movie & file base-name
 *	@exception DevFailed
 */
	void	start_save_movie(const Tango::DevVarDoubleStringArray *);
/**
 * immediately stops the movie saving
 *	@exception DevFailed
 */
	void	stop_save_movie();
/**
 * 
 *	@exception DevFailed
 */
	void	snap();
/**
 * 
 *	@param	argin	[origin_x, origin_y, width, height]
 *	@exception DevFailed
 */
	void	set_roi(const Tango::DevVarLongArray *);
/**
 * 
 *	@return	[origin_x, origin_y, width, height]
 *	@exception DevFailed
 */
	Tango::DevVarLongArray	*get_roi();
/**
 * 
 *	@exception DevFailed
 */
	void	reset_roi();
/**
 * save the current acquisition parameters to the device Properties
 *	@exception DevFailed
 */
	void	save_settings();
/**
 *
 *  @exception DevFailed
 */
	void  reset_camera();
/**
 * Retrieves information about the grabber plugin used
 *	@return	
 *	@exception DevFailed
 */
	Tango::DevString	get_plugin_info();

/**
 *	Read the device properties from database
 */
	 void get_device_property();
//@}

	//	Here is the end of the automatic code generation part
	//-------------------------------------------------------------	



protected :	
	//	Add your own data members here
	//-----------------------------------------

  GrabAPI::SharedImage*         last_image;
  GrabAPI::GrabberTask*         grabber_task;
  GrabAPI::MovieWriterTask*     movie_writer_task;
  
  bool is_saving_movie;
  std::string movie_remaining_time;
  char* movie_remaining_time_cc; 

  std::string plugin_info;

  void register_plugin_properties( GrabAPI::IGrabber* grabber );

  void notify_image_change(const GrabAPI::SharedImage* gimage);

  /*
  yat4tango::PlugInHelper*      plugin_helper;
  yat4tango::DynamicAttrHelper* dyn_attr_helper;
  */
public:

  /// @warn This function assumes that you have the device
  /// lock, it does not try to lock it!
  GrabAPI::SharedImage* get_last_image() const throw (yat::Exception);

  /// Can I just move the signal stuff to GrabberTask? Bad idea, having the
  /// signal at ImgGrabber ensures that if you "Init" the device the
  /// signal remains connected...
  typedef yat::Signal<const GrabAPI::SharedImage*, yat::Mutex> SignalType;
  SignalType image_available_observer_sig;
  typedef SignalType::Slot SlotType;

  SlotType image_change_notify_slot;
  SlotType movie_frame_write_slot;

  void add_image_available_observer(SlotType ob);
  void del_image_available_observer(SlotType ob);
};

}	// namespace_ns

#endif	// _IMGGRABBER_H
